import numpy as np

# dibuja histograma de pesos escalados
def plot_hist_layers(weights, hist_bins):
    import matplotlib.pyplot as plt
    print()
    n_layers = len(weights)
    title_name = "Histogram Layers"
    for i in range(n_layers):
        # plt.figure(i)
        tmp = plt.hist(weights[i].flatten(), bins=hist_bins, label=f'Layer {i}')
        plt.title(title_name)
    plt.legend()
    

def plot_hist_perc(x, bins, xlabel, ylabel, title):
    from matplotlib.ticker import PercentFormatter
    import matplotlib.pyplot as plt
    
    plt.hist(x, weights=np.ones(len(x)) / len(x), bins=bins)
    plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    # plt.show()
    
    