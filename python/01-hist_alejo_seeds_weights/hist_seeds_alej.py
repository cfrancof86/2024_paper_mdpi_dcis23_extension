#%%
#%%[markdown]
# ## PLOT THE SEEDS FOR ORIGINAL WORK
# 

bins = 100
"""-----------------------------------"""
import numpy as np

def order_nparray_by_column(array, column, reverse=False):
    array = sorted(list(array), key=lambda a_entry: a_entry[column], reverse=reverse) 
    return np.array(array)

def accumulate(array):
    from itertools import accumulate
    import operator
    return np.array(list(accumulate(array, operator.add)))


plt.rc('font', size=12)

w_alejo = np.genfromtxt("Input/seeds_scores_alejo.csv",delimiter=',')
w_original = np.genfromtxt("Input/seeds_scores_cnn_v1.csv",delimiter=',')
w_original = w_original[:,1:3]

# ordena
w_alejo = order_nparray_by_column(w_alejo, 1, True)
w_original = order_nparray_by_column(w_original, 1, True)

import matplotlib.pyplot as plt
w_a = w_alejo[:,1]
w_o = w_original[:,1]/100

# Calcula acumulado
hist = np.histogram(w_o, bins=bins)
accum = accumulate(hist[0]/np.sum(hist[0]))
accum = np.concatenate([np.zeros(1), accum])*100

# plot
fig, ax1 = plt.subplots()

ax1.hist(w_o, bins=bins, alpha=0.3, facecolor='blue', edgecolor='black', linewidth=1.0, 
         label=f"SCFPGA22")
ax1.legend()
ax1.set_xlabel("Accuracy")
ax1.set_ylabel('Number of Seeds')

ax2 = plt.twinx()
ax2.plot(hist[1], accum, color='red')
ax2.set_ylabel('Cumulative')
ax2.set_yticks(np.linspace(0, 100, 11))  # Colocar 11 ticks en el eje y derecho
ax2.set_yticklabels(['{}%'.format(int(i*100)) for i in np.linspace(0, 1, 11)])  # Etiquetas en porcentaje

punto_a_resaltar = (0.95,80)
# plt.plot([punto_a_resaltar[0], punto_a_resaltar[0]], [1, punto_a_resaltar[1]], 'r--', label='Línea de cursor X')
# plt.plot([1.5, punto_a_resaltar[0]], [punto_a_resaltar[1], punto_a_resaltar[1]], 'r--', label='Línea de cursor Y')

plt.annotate(f'Acc = 0.95', 
             xy=punto_a_resaltar, xytext=(0.7, 90),
             arrowprops=dict(facecolor='red', shrink=0.05),
             fontsize=12, color='red', ha='center')
plt.plot(punto_a_resaltar[0], punto_a_resaltar[1], 'r.', markersize=10)

# plt.tight_layout()
# # plt.show()


plt.savefig("Output/hist_accuracy_seeds_original.pdf")


#%%[markdown]
# ## PLOT HIST THIS WORK
#
plt.hist(w_a, bins=bins, alpha=0.3, label=f"This work")
plt.legend()
plt.xlabel("Accuracy (%)")
plt.savefig("Output/hist_accuracy_seeds_alejo.pdf")

#%%[markdown]
# # PLOT COMPARISON HISTOGRAMS
#
thresh = 95
plt.hist(w_o[w_o>thresh], bins=bins, alpha=0.3, label=f"SCFPGA22")
plt.hist(w_a[w_a>thresh], bins=bins, alpha=0.3, label=f"This work")
plt.legend()
plt.xlabel("Accuracy (%)")
plt.savefig("Output/hist_accuracy_seeds.pdf")
#%%[markdown]
# # TABLA
#
fp_acc_alejo = 98.98
fp_acc_original = 98.6

degrad_thresh_list = [5, 4, 3, 2, 1]
for deg_i in degrad_thresh_list:
    thresh_a = fp_acc_alejo - deg_i
    thresh_o = fp_acc_original - deg_i
    print(f'This work {deg_i=} = {np.sum(w_a>=thresh_a)}')
    print(f'Original  {deg_i=} = {np.sum(w_o>=thresh_o)}')
    print()