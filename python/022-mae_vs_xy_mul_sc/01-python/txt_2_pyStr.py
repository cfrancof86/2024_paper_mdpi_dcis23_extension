#%%
"""
Pasos para uso:
    1. poner el fichero de texto que queremos converitr
    en el mismo directorio del script.
    
    2. pegar el string generado en el programa donde queramos usarlo.


@Template:
    import txt_2_pyStr
    txt_2_pyStr.txt_2_pyStr("cnn_sc.vhd", "cnn_sc_txt.txt")
"""
def txt_2_pyStr(fileNamevhdl, fileNametxt):
    f = open(fileNamevhdl)
    vhd_str = f.read()
    vhdl_str_split = vhd_str.split('\n')
    py_str = "strFile1 = \"\"\n" 
    for i in vhdl_str_split:
        i = i.replace('\\', '\\\\')
        i = i.replace('{', '{{')
        i = i.replace('}', '}}')
        py_str += "strFile1 += f\'" + i + "\\n\'\n"
        
    # guarda como fichero de texto
    import os
    try:
        os.stat(fileNamevhdl)
    except:
        os.mkdir(fileNamevhdl)   
    f = open (fileNametxt,'w')
    f.write(py_str)
    f.close()
    
    

txt_2_pyStr("latex_table_original.txt", "latex_table_python_in.txt")

#%%

i = f'{{hola}}'