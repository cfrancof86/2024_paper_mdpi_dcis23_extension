#%%
%load_ext autoreload
%autoreload 2
#%%
""" USER define"""
# font = 'Times New Roman'

# from matplotlib import rc
# rc('font',**{'family':font, 'size': 16})

#%%
import numpy as np

def B2S(x, rng):
	return  x>rng

def S2B(x):
	return  np.sum(x)/len(x)

def S2BIP(x):
	return  (np.sum(x)/len(x))*2-1

# PARA AND GATE
def error_stats_AND_gate(x, y, b, N_i, n_iter):
    np.random.seed(3)
    err_abs_arr = []
    err_max_arr = []
    err_min_arr = []    
    max_val = N_i
    err_arr = []
    for x_i in x:
        for y_i in y:
            err_arr_point =[]
            for i in range(n_iter):
                rng1 = np.random.randint(0, max_val-1, size=N_i)
                rng2 = np.random.randint(0, max_val-1, size=N_i)
                x_t = B2S(x_i*max_val, rng1)
                y_t = B2S(y_i*max_val, rng2)
                # PARA AND GATE
                z_t = (x_t & y_t)
                z_bin = np.sum(z_t)
				
                #ground truth:				
                # es la multiplicacion de los valores sin tener
                # en cuenta el error por conversion b2s. Asi que
                z_i = S2B(x_t) * S2B(y_t)
                err = abs(z_bin/max_val - z_i)
                err_arr_point.append(err)
            err_arr_point = np.array(err_arr_point)
            err_max_arr.append(err_arr_point.max())
            err_min_arr.append(err_arr_point.min())
            err_abs_mean = err_arr_point.mean()
            err_abs_arr.append(err_abs_mean)       		

    err_abs_arr = np.array(err_abs_arr)	
    err_abs_arr = np.reshape(err_abs_arr, (len(x),len(y)))
    # estatistics
#    err_max = np.array(err_max_arr).max()
    err_max = np.array(err_abs_arr).max()
    err_min = np.array(err_min_arr).min()
    err_mean = np.array(err_abs_arr).mean()
    err_std = np.array(err_abs_arr).std()
    return err_abs_arr, err_max, err_min, err_mean, err_std
	

# PARA XNOR GATE
def error_stats_XNOR_gate(x, y, b, N_i, n_iter):
    
    np.random.seed(3)
    err_abs_arr = []
    err_max_arr = []
    err_min_arr = []
    
    max_val = N_i
    err_arr = []
    for x_i in x:
        for y_i in y:		
            err_arr_point =[]					
            for i in range(n_iter):					
                rng1 = np.random.randint(0, max_val-1, size=N_i)
                rng2 = np.random.randint(0, max_val-1, size=N_i)
                x_t = B2S(x_i*max_val, rng1)
                y_t = B2S(y_i*max_val, rng2)
                # PARA xnor GATE
                z_t = ~(x_t ^ y_t)
                z_bin = S2BIP(z_t)
                #ground truth:
                # es la multiplicacion de los valores sin tener
                # en cuenta el error por conversion b2s. Asi que
                z_i = S2BIP(x_t) * S2BIP(y_t)
                err = abs(z_bin - z_i)
                err_arr_point.append(err)						
            err_arr_point = np.array(err_arr_point)
				# estaditicas
            err_max_arr.append(err_arr_point.max())
            err_min_arr.append(err_arr_point.min())
            err_abs_mean = err_arr_point.mean()
            # para plot
            err_abs_arr.append(err_abs_mean)

    err_abs_arr = np.array(err_abs_arr)	
    err_abs_arr = np.reshape(err_abs_arr, (len(x),len(y)))
    # estatistics
#    err_max = np.array(err_max_arr).max()
    err_max = np.array(err_abs_arr).max()
    err_min = np.array(err_min_arr).min()
    err_mean = np.array(err_abs_arr).mean()
    err_std = np.array(err_abs_arr).std()
    return err_abs_arr, err_max, err_min, err_mean, err_std
						
#%%
"""
PLOTTING

"""
name_file_AND = '022-mae_vs_xy_AND.pdf'
name_file_XNOR = '022-mae_vs_xy_XNOR.pdf'

n_iter = 1000
b = 8
N = 1024
points_in_axes = 10

#%%
"""
plot AND

"""

x = np.linspace(0, 1, num=points_in_axes)
y = np.linspace(0, 1, num=points_in_axes)
err_abs_arr_AND, err_max_AND, err_min_AND, err_mean_AND, err_std_AND = error_stats_AND_gate(x, y, b, N, n_iter)

#%%
x = np.linspace(0, 1, num=points_in_axes)
y = np.linspace(0, 1, num=points_in_axes)
plt_title = ""
x_label = "x" 
y_label = "y"
z_label = "MAE" 
x_ticks = [0, 0.5, 1]
y_ticks = x_ticks
x_tick_Labels = x_ticks
y_tick_labels = x_tick_Labels
filename = name_file_AND

angle = (20,-40)
zlimits = (0, 0.03)

colorbar = False

import my_plots
my_plots.plot_3D(1, x, y, err_abs_arr_AND, plt_title, x_label, y_label, z_label, 
                 x_ticks, y_ticks, x_tick_Labels, y_tick_labels, 
				     filename, angle, colorbar, zlimits)


#%%
"""
plot XNOR

"""
x = np.linspace(0, 1, num=points_in_axes)
y = np.linspace(0, 1, num=points_in_axes)
err_abs_arr_XNOR, err_max_XNOR, err_min_XNOR, err_mean_XNOR, err_std_XNOR = error_stats_XNOR_gate(x, y, b, N, n_iter)

#%%
x = np.linspace(-1, 1, num=points_in_axes)
y = np.linspace(-1, 1, num=points_in_axes)

plt_title = ""
x_label = "x"
y_label = "y"
z_label = "MAE"
x_ticks = [-1, -0.5,  0, 0.5, 1]
y_ticks = x_ticks
x_tick_Labels = x_ticks
y_tick_labels = x_tick_Labels
filename = name_file_XNOR
angle = (20,-40)
zlimits = (0, 0.03)
colorbar = False

import my_plots
my_plots.plot_3D(2, x, y, err_abs_arr_XNOR, plt_title, x_label, y_label,z_label, 
                 x_ticks, y_ticks, x_tick_Labels, y_tick_labels, 
				 filename, angle, colorbar, zlimits)


#%%
"""
    Table 

    Calculus

"""
n_iter = 1000
b = [8]
N = [32,64,128,256,1024]

x = np.linspace(0, 1, num=20)
y = np.linspace(0, 1, num=20)

table = []
for N_i in N:
    table.append(error_stats_AND_gate(x, y, b, N_i, n_iter))
    table.append(error_stats_XNOR_gate(x, y, b, N_i, n_iter))

import my_pickle
my_pickle.pickle_save("N.pickle", N) 
my_pickle.pickle_save("table.pickle", table)    
#%%
"""
    Table print in Latex format

    Calculus

"""
import my_pickle
N = my_pickle.pickle_load("N.pickle")
table = my_pickle.pickle_load("table.pickle") 

# table indexes
# err_abs_arr, err_max, err_min, err_mean, err_std
max_idx = 1
mean_idx = 3
std_idx = 4

k = 1000 # scale data
format_data = '.1f'

strFile1 = ""
strFile1 += f'\\begin{{table}}[]\n'
strFile1 += f'\\centering\n'
strFile1 += f'\\caption{{}}\n'
strFile1 += f'\\label{{tab:my-table}}\n'
strFile1 += f'\\begin{{tabular}}{{c|cc|cc|cc}}\n'
strFile1 += f'\\hline\n'
strFile1 += f'\\rowcolor[HTML]{{EFEFEF}} \n'
strFile1 += f'\\cellcolor[HTML]{{EFEFEF}} & \\multicolumn{{2}}{{c|}}{{\\cellcolor[HTML]{{EFEFEF}}\\textbf{{MSE}}}} & \\multicolumn{{2}}{{c|}}{{\\cellcolor[HTML]{{EFEFEF}}\\textbf{{std}}}} & \\multicolumn{{2}}{{c}}{{\\cellcolor[HTML]{{EFEFEF}}\\textbf{{max}}}} \\\\ \\cline{{2-7}} \n'
strFile1 += f'\\rowcolor[HTML]{{EFEFEF}} \n'
strFile1 += f'\\multirow{{-2}}{{*}}{{\\cellcolor[HTML]{{EFEFEF}}\\textbf{{N}}}} & \\cellcolor[HTML]{{EFEFEF}}AND & \\cellcolor[HTML]{{EFEFEF}}XNOR & AND & XNOR & AND & XNOR \\\\ \\hline\n'

for i in range(0, len(table), 2):
    and_gate = table[i]
    xnr_gate = table[i+1]
    strFile1 += f'{N[int(i/2)]} & '
    strFile1 += f'{and_gate[mean_idx]*k:{format_data}} & '
    strFile1 += f'{xnr_gate[mean_idx]*k:{format_data}} & '
    strFile1 += f'{and_gate[std_idx]*k:{format_data}} & '
    strFile1 += f'{xnr_gate[std_idx]*k:{format_data}} & '
    strFile1 += f'{and_gate[max_idx]*k:{format_data}} & '
    strFile1 += f'{xnr_gate[max_idx]*k:{format_data}} '
    strFile1 += f' \\\\ \\hline\n '
    
strFile1 += f'\\end{{tabular}}\n'
strFile1 += f'\\end{{table}}\n'



f = open ('_latex_table_out.txt','w')
f.write(strFile1)
f.close()










# %%
