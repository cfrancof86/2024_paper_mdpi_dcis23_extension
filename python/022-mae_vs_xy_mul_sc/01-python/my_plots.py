# -*- coding: utf-8 -*-
"""
Created on Wed May 15 08:19:17 2019

@author: admin
"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

"""----------------------------------------------------------------------------
@Description:
    Grafica un mesh
    
@Arguments:
    figureNum : Numero de figura que queremos graficar.
    x_tick_Labels : array donde estan los labels del ejex. Debe de tener
                    el mismo tamaño que una de las dimensiones de z_mesh.
@returns:

@Template:    
     my_plots.plot_mesh(1, err_mesh_seeds_fix,
             "Error for Best Seeds couple. \nSeed_x = " + str(seed1_minErr) +  "\nSeed_y = " + str(seed2_minErr),
             "Input x",
             "Input y",
             "Error",
             tick_Labels,
             tick_Labels)
-------------------------------------------------------------------------------"""
def plot_mesh(figureNum, z_mesh, plt_title, x_label, y_label, z_label, 
              x_tick_Labels, y_tick_labels):

    plt.figure(figureNum)
    fig, ax = plt.subplots()
    im = ax.imshow(z_mesh)
    
    if(len(x_tick_Labels)<32):
        # hayo el numero de intervalos en los axis
        num_ticks_axis = z_mesh.shape[0]
        # genero los ticks de los laterales para x  e y
        ax.set_xticks(np.arange(num_ticks_axis))
        ax.set_yticks(np.arange(num_ticks_axis))
    
        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=90, ha="right",
                 rotation_mode="anchor")
    
        # cambio nombre a los labels de ticks
        ax.set_xticklabels(x_tick_Labels)
        ax.set_yticklabels(y_tick_labels)
        
    # nombre de cada eje
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    # titulo
    ax.set_title(plt_title)
    cbar = fig.colorbar(im)
    cbar.set_label(z_label)
    #cbar.ax.set_title(z_label)
    #cbar.ax.set_yticklabels([err_min,err_max])

    plt.show()
"""----------------------------------------------------------------------------
@Description:
    Grafica un 3D 
    
@Arguments:
    plt_angle : angulo de rotacion 3D. Es un tuple o array de 2 posiciones
                indicando el valor de rotacion.
                    [0] = rotacion Vertical
                    [1] = rotacion Horizontal
                Dflt = (30,-45)
    args: limite de eje z, descrito en tuple : (0, 1)
@returns:

@Template:    


-------------------------------------------------------------------------------"""   
def plot_3D(figureNum,  x, y, z_mesh, plt_title, x_label, y_label, z_label, 
            x_ticks, y_ticks, x_tick_labels, y_tick_labels, 
            filename, plt_angle, colorbar, *args):

        
     from matplotlib import cm
     # convierte en mesh para que se pueda graficar en 3D
     x_mesh, y_mesh = np.meshgrid(x, y) 
    
     fig = plt.figure(figureNum)
   
     ax = plt.axes(projection='3d')
     cmap = 'magma'
     fig3d = ax.plot_surface(x_mesh, y_mesh,z_mesh, cmap=cmap, linewidth=2, antialiased=True)
    #  fig3d = ax.plot_wireframe(x_mesh, y_mesh,z_mesh, cmap=cm.coolwarm, linewidth=0, antialiased=False)

    
 #    ax.contour(x_mesh, y_mesh, z_mesh, zdir='z', offset=np.min(z_mesh))
 #    ax.contour(x_mesh, y_mesh, z_mesh, zdir='x', offset=np.min(x_tick_Labels))
 #    ax.contour(x_mesh, y_mesh, z_mesh, zdir='y', offset=np.max(y_tick_labels))

     # nombre de cada eje
     ax.set_xlabel(x_label)
     ax.set_ylabel(y_label)
     ax.set_zlabel(z_label)
     # limits for z axis
     if args:
          ax.set_zlim(args[0])
     # titulo
     ax.set_title(plt_title)
     ax.view_init(plt_angle[0], plt_angle[1])
     
     # cambio nombre a los labels de ticks
     ax.set_xticks(x_ticks)
     ax.set_yticks(y_ticks)
     ax.set_zticks([0, 0.01, 0.02, 0.03])
     ax.set_xticklabels(x_tick_labels)
     ax.set_yticklabels(y_tick_labels)

     if colorbar : 
         fig.colorbar(fig3d, shrink=0.5, aspect=5)
    
     plt.savefig(filename)
     plt.show() 

"""----------------------------------------------------------------------------
@Description:
    Grafica un 3D con lineas. 
    
@Arguments:
    plt_angle : angulo de rotacion 3D. Es un tuple o array de 2 posiciones
                indicando el valor de rotacion.
                    [0] = rotacion Vertical
                    [1] = rotacion Horizontal
                Dflt = (30,-45)

@returns:

@Template:    

-------------------------------------------------------------------------------""" 
def plot_3D_wireframe(figureNum, z_mesh, plt_title, x_label, y_label, z_label, 
                      x_tick_Labels, y_tick_labels, plt_angle):

    # convierte en mesh para que se pueda graficar en 3D
#    x_mesh, y_mesh = np.meshgrid(x_tick_Labels, y_tick_labels) 
    x_mesh, y_mesh = np.meshgrid(x_tick_Labels, y_tick_labels) 
    
    fig = plt.figure(figureNum)
   
    ax = plt.axes(projection='3d')
    fig3d = ax.plot_wireframe(x_mesh, y_mesh,z_mesh)

    # nombre de cada eje
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    # titulo
    ax.set_title(plt_title)
    
    ax.view_init(plt_angle[0], plt_angle[1])


    plt.show()
"""----------------------------------------------------------------------------
@Description:
    Grafica 4 planos. 
    
@Arguments:


@returns:

@Template:    

-------------------------------------------------------------------------------""" 
def plot_3D_planes_surface(figureNum, z_mesh, plt_title, x_label, y_label, z_label, 
                      x_tick_Labels, y_tick_labels):
    from matplotlib import cm
    # convierte en mesh para que se pueda graficar en 3D
#    x_mesh, y_mesh = np.meshgrid(x_tick_Labels, y_tick_labels) 
    x_mesh, y_mesh = np.meshgrid(x_tick_Labels, y_tick_labels) 
    
    fig = plt.figure(figureNum)
    
    # figura 1
    # set up the axes for the first plot
    ax = fig.add_subplot(2, 2, 1, projection='3d')
    ax.plot_surface(x_mesh, y_mesh,z_mesh, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # nombre de cada eje
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    # titulo
    ax.view_init(90, 0)
    
    # figura 2
    # set up the axes for the first plot
    ax = fig.add_subplot(2, 2, 2, projection='3d')
    ax.plot_surface(x_mesh, y_mesh,z_mesh, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # nombre de cada eje
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.view_init(0, 90)
    
    # figura 2
    ax = fig.add_subplot(2, 2, 3, projection='3d')
    ax.plot_surface(x_mesh, y_mesh,z_mesh, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # nombre de cada eje
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.view_init(0, 0)
    
    # figura 3
    ax = fig.add_subplot(2, 2, 4, projection='3d')
    ax.plot_surface(x_mesh, y_mesh,z_mesh, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    # nombre de cada eje
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
  
    ax.view_init(30, -45)
    
    plt.show()
"""----------------------------------------------------------------------------
@Description:
    Grafica histogramas de un array. Debe de ser una lista
    
@Arguments:
    x : debe de ser una lista

@returns:

@Template:    
    from my_plots import plot_hist_figures
    plot_hist_figures(x, 100)
-------------------------------------------------------------------------------""" 
def plot_hist_figures(x, bins, title_name):
    for i in range(len(x)):
        plt.figure(i)
        plt.hist(x[i], bins = bins)
        plt.title(title_name + str(i))

