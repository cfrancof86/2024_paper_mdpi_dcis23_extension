#%%
import numpy as np
import my_pickle 

def weightsAlej_2_myWeights(wa):
    # Convierte de diccionario a lista
    w_test = []
    w_test.append(wa["conv1"])
    w_test.append(wa["conv2"])
    w_test.append(wa["fc1"])
    w_test.append(wa["fc2"])
    w_test.append(wa["fc3"])

    w_test[0] = np.transpose(w_test[0], [2,3,1,0])  
    w_test[1] = np.transpose(w_test[1], [2,3,1,0])  
    w_test[2] = np.transpose(w_test[2], [1,0])  
    w_test[3] = np.transpose(w_test[3], [1,0])  
    w_test[4] = np.transpose(w_test[4], [1,0])  

    return w_test



def plot_hist_layers(weights, hist_bins):
    import matplotlib.pyplot as plt
    print()
    n_layers = len(weights)
    # plt.figure(1, figsize=(40, 5), dpi=80)
    # plt.figure(1, figsize=(40, 5), dpi=80)
    for i in range(n_layers):
        # plt.figure(i)
        # plt.subplot(1,5,i+1)

        w_flat = weights[i].flatten()
        w_max = np.max(abs(w_flat))
        w_norm = w_flat / w_max

        # tmp = plt.hist(w_norm, bins=hist_bins, alpha=0.5)
        # plt.title(f'Layer {i+1}')

        # plt.grid(which='both', axis='y', linestyle='--', linewidth=0.7, color='silver', alpha=0.6)
        from myHistograms import plot_hist_perc
        plot_hist_perc(w_norm, hist_bins, "", "", "")

def plot_hist_perc(weights, bins):
    from matplotlib.ticker import PercentFormatter
    import matplotlib.pyplot as plt

    n_layers = len(weights)
    # plt.figure(1, figsize=(8, 4), dpi=80)

    for i in range(n_layers):
        w_flat = weights[i].flatten()
        w_max = np.max(abs(w_flat))
        w_norm = w_flat / w_max

        plt.hist(w_norm, weights=np.ones(len(w_norm)) / len(w_norm), 
                 bins=bins, alpha=0.3, label=f"Layer {i+1}")
        plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
        plt.legend()
    x_ticks = np.linspace(-1,1,5)
    # y_ticks = np.linspace(-1,1,5)
    plt.xticks(x_ticks)  
    plt.xlabel('Weight Values')
    plt.ylabel('Frequency')
    plt.tight_layout()
    plt.savefig("Output/hist_weights_alejandro.pdf")






#%%
from matplotlib import rc
rc('font',**{'size': 14})
w = my_pickle.pickle_load("Input/w_scaled_sc.pickle")
w = np.array(w)

# w_conv = []
# w_conv.append(w[0])
# w_conv.append(w[1])
# # dibuja histograma de pesos 
# plot_hist_perc(w_conv, 40)

# w_ff = []
# w_ff.append(w[2])
# w_ff.append(w[3])
# w_ff.append(w[4])
# plot_hist_perc(w_ff, 40)

plot_hist_perc(w, 40)
