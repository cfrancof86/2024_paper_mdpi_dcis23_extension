# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 11:19:34 2020

@author: admin
"""

import pickle
import numpy as np
"""-------------------------------------------------------
Template:
    import my_pickle
    my_pickle.pickle_save("x_test.pickle", x_test)    
-------------------------------------------------------"""
def pickle_save(file_name, obj):
    pickle_out = open(file_name, "wb")
    pickle.dump(obj, pickle_out)
    pickle_out.close()
"""-------------------------------------------------------
Template:
    import my_pickle
    x_test = my_pickle.pickle_load("x_test.pickle")    
-------------------------------------------------------"""
def pickle_load(file_name):
    pickle_in = open(file_name, "rb")
    obj = pickle.load(pickle_in)
    pickle_in.close()
    return obj

""" 
nueva forma de leer los pesos del modelo keras 
cuyos pesos los exporte en pickle
"""
def read_weights_pickle(filename):
    import my_pickle
    w = my_pickle.pickle_load(filename)
    # como no tiene bias, creo lista de 0 
    # con el shape adecuado
    b =[]
    for layer_i in range(len(w)):
        b.append(np.zeros(w[layer_i].shape[-1]))
    return w, b